import org.yosy.Role
import org.yosy.User
import org.yosy.UserRole

class BootStrap {
	def springSecurityService

	def init = { servletContext ->
		// encode the password only if you're using an older version of the 
		// spring-security-core plugin and you don't have encoding logic in 
		// your "User" domain class: 
		//		String password = 'Yos!2011'
		//
		//		def roleAdmin = new Role(authority: 'ROLE_ADMIN').save()
		//		def roleUser = new Role(authority: 'ROLE_USER').save()
		//		def user = new User(username: 'user', password: password, enabled: true).save()
		//		def admin = new User(username: 'admin', password: password, enabled: true).save()
		//
		//		UserRole.create(user, roleUser)
		//		UserRole.create(admin, roleUser)
		//		UserRole.create(admin, roleAdmin, true)
	}
	def destroy = {
	}
}
