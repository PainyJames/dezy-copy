dataSource {
	pooled = true
	driverClassName = "com.mysql.jdbc.Driver"
	//username = "yosydb_user"
	//password = "Yos!2011"
	username = "dezyuser"
	password = "Yosy2012Dezy"
	dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
	dbCreate = "update"
 }
hibernate{    
	cache.use_second_level_cache = "true"
    cache.use_query_cache = "true"    
	cache.provider_class = 'org.hibernate.cache.EhCacheProvider'

}
 // environment specific settings
 environments {
		development {
				dataSource {
						dbCreate = "update"//"create-drop" // one of 'create', 'create-drop','update'
						//url = "jdbc:mysql://ec2-75-101-156-134.compute-1.amazonaws.com:3306/yosydb"
						url = "jdbc:mysql://my03.winhost.com:3306/mysql_38969_dezy"
				}
		}
		test {
				dataSource {
						dbCreate = "update"
						//url = "jdbc:mysql://ec2-75-101-156-134.compute-1.amazonaws.com:3306/yosydb"
						url = "jdbc:mysql://my03.winhost.com:3306/mysql_38969_dezy"
				}
		}
		production {
				dataSource {
						dbCreate = "update"
						//url = "jdbc:mysql://ec2-75-101-156-134.compute-1.amazonaws.com:3306/yosydb"
						url = "jdbc:mysql://my03.winhost.com:3306/mysql_38969_dezy"
				}
		}
 }