import org.yosy.VotesService;
import org.yosy.WhisesService
import org.yosy.UsersService
import org.yosy.CommentsService;
import org.yosy.CategoryService;
import org.yosy.LocationService;

// Place your Spring DSL code here
beans = {    
	whisesService(WhisesService) 
	usersService(UsersService) 
	votessService(VotesService)
	commentsService(CommentsService)
	locationService(LocationService)
	categoryService(CategoryService)
}
