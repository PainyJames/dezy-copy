import java.net.Authenticator.RequestorType;
import org.yosy.EstadoUsuario;
import org.yosy.Usuario

class Filters {

	def filters = {
		all(controller:'index', action:'save|vote|createComment') {
			before = {
				if(!session.user || session.estadoUsuario != EstadoUsuario.REGISTRADO) {	
					response.status = 401 // no autorizado
					return false
				}
				else {
					return true
				}
			}
			after = {
			}
			afterView = {
			}
		}
	}
}
