package org.yosy

import grails.converters.JSON
import groovy.swing.impl.DefaultAction;
//import grails.plugins.springsecurity.Secured

class IndexController {

	def defaultAction = 'index'
	def whisesService
	def votesService
	def commentsService
	def categoryService
	def locationService
	def usersService
	org.yosy.utils.Utilities utilities

	def index = {
		def localidad = locationService.list()
		def categoria = categoryService.list()
		render(view:'../index', model:['template':'makeWhis','localidad':localidad,'categoria':categoria])
	}
	def disclaimer = {
		render(view:'../index', model:['template':'disclaimer'])
	}
	def privacy_policy = {
		render(view:'../index', model:['template':'privacy_policy'])
	}

	def list = {
		render(view:'../index', model:['template':'whisesList','deseoInstanceList': whisesService.listWhises(params), 'deseoInstanceTotal': whisesService.countWhises()])
	}

	def whisDetail = {
		def idDeseo = params['idWhis']
		def deseo = whisesService.get(idDeseo)
		render(view:'../index', model:['template':'whisDetails','deseoInstance': deseo])
	}

	//@Secured(['ROLE_USER'])
	def save = {
		def email = session.user
		if(whisesService.save(params, email))
			render(text:'Deseo creado',contentType:'text/plain')
		else
			response.status = 401
	}

	def vote =
	{
		def email = session.user
		def idDeseo = new Long(params['id'])
		def returnText = votesService.vote(email, idDeseo,utilities)
		render(text:returnText,contentType:'text/plain')
	}

	def createComment =
	{
		def email = session.user
		if(commentsService.save(params, email,utilities))
		{
			def comments = commentsService.listComments(params)
			render(view:'../comments/_commentsWhis', model:['comments':comments])
		}
		else
			response.status = 401
	}

	def listCommentsAfterDate =
	{
		def comments
		if (params['date'] != "")
			comments = commentsService.listCommentsAfterDate(params)
		else
		{
			params['id'] = params['idWish']
			comments = commentsService.listComments(params)
		}
		render comments as JSON
	}

	def autocompleteLocation =
	{
		def returnText = '['
		def locations = locationService.autocompleteLocation(params)
		for(def location in locations)
			returnText += '"'+location.nombre+'",'
		render(text:returnText+'""]',contentType:'text/plain')
	}

	def autocompleteCategory =
	{
		def returnText = '['
		def categories = categoryService.autocompleteCategory(params)
		for(def category in categories)
			returnText += '"'+category.nombre+'",'
		render(text:returnText+'""]',contentType:'text/plain')
	}

	def existsEmail =
	{
		def email = params["email"];
		def exists = usersService.existsEmail(email)
		render(text:exists,contentType:"text/plain")
	}

	def existsNick =
	{
		def nick = params["nick"];
		def exists = usersService.existsNick(nick)
		render(text:exists,contentType:"text/plain")
	}

	def contact =
	{
		render(view:'../index', model:['template':'contact'])
	}

	def grantWhis =
	{
		render(view:'../index', model:['template':'grantWhis'])
	}

}
