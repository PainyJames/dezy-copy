package org.yosy

import groovy.swing.impl.DefaultAction;

class UserController {

	def usersService
	org.yosy.utils.Utilities utilities

	def save = {
		redirect(controller: 'index', action: 'list')
	}

	def activate = {
		if(usersService.activate(params, utilities))
			render(view:'../index', model:['template':'../msg','msg':'Registro correcto'])
		else {
			response.status = 401
			render(view:'../index', model:['template':'../msg','msg':'Registro incorrecto'])
		}
	}

	def register = {
		if(usersService.register(params, utilities))
			render(text:'Registro correcto',contentType:'text/plain')
		else {
			render(text:'Registro incorrecto',contentType:'text/plain')
			response.status = 401
		}
	}

	def login = {
		def usuarioInstance = usersService.login(params)
		if(usuarioInstance != null && usuarioInstance.estadoUsuario == EstadoUsuario.REGISTRADO) {

			session.user = usuarioInstance.email
			session.estadoUsuario = usuarioInstance.estadoUsuario
			session.userId = usuarioInstance.id
			render(text:'Login correcto',contentType:'text/plain')
		}
		else {
			response.status = 401 // no autorizado
			render(text:'Login incorrecto',contentType:'text/plain')
		}
	}
	
	def logout = {
		session.user = null
		render(text:'Logout correcto',contentType:'text/plain')
	}
	
	def listUserVotes = {
		def usuarioList = usersService.listUserVotes(params)
		render(view:'../votes/listVotesWhis', model:['usersInstanceList':usuarioList])
	}
	
}
