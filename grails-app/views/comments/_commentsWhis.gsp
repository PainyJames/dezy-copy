<tr>
	<th style="width:25%;">
		Autor								
	</th>
	<th style="width:50%;">
		Comentario								
	</th>
	<th style="width:25%;">
		Fecha								
</th>
</tr>
<g:each in="${comments}" status="u" var="commentInstance">
	<tr class="commentRow">
		<td style="width:25%;">
			${commentInstance.autor}
		</td>
		<td style="width:50%;">
			${commentInstance.texto}
		</td>
		<td style="width:25%;">
			${commentInstance.fecha.format("dd/MM/yyyy - hh:mm")}
		</td>
	<tr>
	<g:if test="${u == comments.size() - 1}">
	<tr>
	<td style="display:none">
		<input id="lastDateComment" value="${commentInstance.fecha.format("dd/MM/yyyy - hh:mm")}" />
	</td>
	</tr>
	</g:if>
</g:each>
<tr>
<td>
<span>Actualizar</span>
</td>
</tr>