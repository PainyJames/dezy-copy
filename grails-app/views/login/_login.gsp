<%@ page import="org.yosy.Usuario"%>
<div class="centerPopUpDiv" style="margin-left: 110px;">
	<div class="nav" style="color: black;">
		<span class="menuButton linkCerrar"
			onclick="hideLogin(); document.getElementById('div_login').style.display = 'block'; document.getElementById('div_registro').style.display = 'none'; return false; "> <g:message
				code="default.close.label" />
		</span>
	</div>
	<div class="body">
		<div id="div_login" style="float: left; margin: 0 0 0 8%; width: 80%;">
			<h1 class="ttl_login">
				<g:message code="default.login.label" />
			</h1>
			<g:if test="${flash.message}">
				<div class="message"
					style="color: red; margin: 10px 0px; font-weight: bold;">
					${flash.message}
				</div>
			</g:if>
			<g:hasErrors bean="${usuarioInstance}">
				<div class="errors">
					<g:renderErrors bean="${usuarioInstance}" as="list" />
				</div>
			</g:hasErrors>
			<div>
				${message}
			</div>
			<g:formRemote url="[ controller: 'user', action: 'login']"
				name="login" on401="return false;"
				onSuccess="hideLogin(); loginLink(); return false;"
				onFailure="ShowLoginMsg('Login incorrecto'); return false;">
				<table>
					<tbody>
						<tr class="prop">
							<td valign="top" class="name"><label for="email"><g:message
										code="usuario.email.label" default="Email" /></label></td>
							<td valign="top"
								class="value ${hasErrors(bean: usuarioInstance, field: 'email', 'errors')}">
								<g:textField name="email" value="${usuarioInstance?.email}" />
							</td>
						</tr>
						<tr class="prop">
							<td valign="top" class="name"><label for="password"><g:message
										code="usuario.password.label" default="Contraseña" /></label></td>
							<td valign="top"
								class="value ${hasErrors(bean: usuarioInstance, field: 'password', 'errors')}">
								<g:passwordField name="password" value="" type="password" />
							</td>
						</tr>
						<tr>
							<td colspan=2>
								<span class="linkBlue" onclick="document.getElementById('div_login').style.display = 'none'; document.getElementById('div_registro').style.display = 'block';">
									Regístrate
								</span>
								</br>
								<div class="buttons">
									<span class="button" style="display:none;"> 
										<g:submitToRemote url="[controller: 'user', action: 'login']" id="botonLogin" onFailure="ShowLoginError('ErrorLogin'); return false;" onSuccess="hideLogin(); showResponse('Login correcto'); loginLink();" value="${message(code: 'default.button.login.label', default: 'Login')}" />
									</span>
									<img style="width: 148px; height: 37px; cursor: pointer; margin: 5% 0 0 0;"  src="${resource(dir:'images',file:'boton.png')}">
									<div class="btn_login" onclick="$('#botonLogin').click()">
										Entrar
									</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</g:formRemote>
			<div style=" display:none; color: red; margin: 10px 0px; font-weight: bold;">
				<label id="lblErrorLogin" style="display: none;"> <g:message
						code="errors.login.errorpassemail"
						default="El email o la contraseña no son correctos." />
				</label> <label id="lblErrorPasswordVacio" style="display: none;"> <g:message
						code="errors.login.erroremptypass"
						default="El campo de contraseña no puede estar vacio." />
				</label>
			</div>
		</div>
		<div id="div_registro" style="float: left; margin: 0 0 0 8%; width: 80%; display: none;">
			<h1 class="ttl_register">
				<g:message code="default.register.label" />
			</h1>
			<g:formRemote url="[ controller: 'user', action: 'register']"
				name="register" on401="return false;"
				onSuccess=" hideLogin(); return false;">
				<div class="dialog">
					<table>
						<tbody>
							<tr class="prop">
								<td valign="top" class="name"><label for="email"><g:message
											code="usuario.email.label" default="Email" /></label></td>
								<td valign="top"
									class="value ${hasErrors(bean: usuarioInstance, field: 'email', 'errors')}">
									<g:textField class="emailRegistro" name="email" value="${usuarioInstance?.email}" />
									<img src="images/loader.gif" alt="" id="imgLoadingEmail" style="display:none;" />
								</td>
							</tr>
							<tr class="prop">
								<td valign="top" class="name"><label for="nombre"><g:message
											code="usuario.nick.label" default="Nick" /></label></td>
								<td valign="top"
									class="value ${hasErrors(bean: usuarioInstance, field: 'nick', 'errors')}">
									<g:textField name="nombre" value="${usuarioInstance?.nombre}" />
									<img src="images/loader.gif" alt="" id="imgLoadingNick" style="display:none;" />
								</td>
							</tr>
							<tr class="prop">
								<td valign="top" class="name"><label for="password"><g:message
											code="usuario.password.label" default="Contraseña" /></label></td>
								<td valign="top"
									class="value ${hasErrors(bean: usuarioInstance, field: 'password', 'errors')}">
									<g:passwordField name="passwordRegistro" value="" type="password" />
								</td>
							</tr>
							<tr class="prop">
								<td valign="top" class="name"><label for="password"><g:message
											code="usuario.passwordRepeat.label"
											default="Repite contraseña" /></label></td>
								<td valign="top" class="value"><g:passwordField
										name="passwordRepeat" value="" type="password" /></td>
							</tr>
							<tr>
								<td colspan="2">
	
									<div class="buttons">
										<span id="spanBtnRegister" class="button" style="display:none;"> 
											<g:submitToRemote id="btnRegister"
												url="[controller: 'user', action: 'register']"
												onFailure="showLogin()"
												onSuccess="hideLogin(); showResponse('Registro completado')"
												value="${message(code: 'default.button.register.label', default: 'Regístrate')}" />
										</span>
										<img style="width: 148px; height: 37px; cursor: pointer; margin: 5% 0 0 43%;" src="${resource(dir:'images',file:'boton.png')}ç">
										<div class="btn_register"  onclick="$('#btnRegister').click()" >
											Regístrate
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<div style="color: red; margin: 10px 0px; font-weight: bold;">
										<label id="lblErrorRegisterCamposVacios" style="display: none;"> <g:message
												code="errors.login.errorregister.camposvacios"
												default="Hay campos vacios." />
										</label>
										<label id="lblErrorRegisterEmail" style="display: none;"> <g:message
												code="errors.login.errorregister.email"
												default="El email no es correcto." />
										</label>
										<label id="lblErrorRegisterPass" style="display: none;"> <g:message
												code="errors.login.errorregisterpassword"
												default="Las contraseñas no coinciden." />
										</label>
										<label id="lblErrorEmailVacio" style="display: none;"> <g:message
												code="errors.login.erroremptyemail"
												default="El campo de email no puede estar vacio." />
										</label> <label id="lblErrorEmailRepetido" style="display: none;"> <g:message
												code="errors.login.erroremailrepeat"
												default="El campo de email está repetido." />
										</label> <label id="lblErrorNickRepetido" style="display: none;"> <g:message
												code="errors.login.errornickrepeat"
												default="El campo de nombre está repetido." />
										</label>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</g:formRemote>
			<g:if test="${flash.message}">
				<div class="message"
					style="color: red; margin: 10px 0px; font-weight: bold;">
					${flash.message}
				</div>
			</g:if>
			<g:hasErrors bean="${usuarioInstance}">
				<div class="errors">
					<g:renderErrors bean="${usuarioInstance}" as="list" />
				</div>
			</g:hasErrors>
			<div>
				${message}
			</div>
		</div>

	</div>
</div>