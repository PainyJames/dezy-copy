<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Dezy</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" href="${resource(dir:'css',file:'main.css')}" />
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.8.20/themes/base/jquery-ui.css" />
		<g:setProvider library="jquery" />
		<script type="text/javascript"
			src="${resource(dir:'js',file:'social.js',absolute:true)}">
		</script>
	</head>
	<body>
		<div id="top">
			<div id="login">
			<div style="float:left;cursor: pointer; height: 70px; width: 300px;" onclick="window.location ='http://www.dezy.es'">
			</div>
				<ul>
					<li class="li_cab ">
						<g:link controller="index" class="link">
							Inicio
						</g:link>
					</li>
					<li class="li_cab izq">
						<g:link controller="index" action="grantWhis" class="link">
							Conceder deseos
						</g:link>
					</li>
					<li class="li_cab">
						<g:link controller="index" action="list" class="link">
							Ver deseos
						</g:link>
					</li>
					<g:if test="${!session.user}">
						<li class="li_cab" id="loginLink" onclick="showLogin(); return false;">
							<span style="cursor: pointer">
								Login
							</span>
						</li>
						<li class="li_cab" id="logoutLink" style="display: none">
							<g:remoteLink controller="user" action="logout" onSuccess="logoutLink(); return false;">
								<span style="cursor: pointer">
									Logout
								</span>
							</g:remoteLink>
						</li>
					</g:if>
					<g:else>
						<li class="li_cab" id="loginLink" onclick="showLogin(); return false;" style="display: none">
							<span style="cursor: pointer">
								Login
							</span>
						</li>
						<li class="li_cab" id="logoutLink">
							<g:remoteLink controller="user" action="logout" onSuccess="logoutLink(); return false;">
								<span style="cursor: pointer">
									Logout
								</span>
							</g:remoteLink>
						</li>
					</g:else>
				</ul>
			</div>
		</div>
		<div id="center">
			<g:render template="$template" />
		</div>
		<div id='backgroundDivPopUp' style='display: none'></div>
		<div id="loginPopUp" style="display: none;">
			<g:render template="../login/login" />
		</div>
		<div id="responsePopUp" style="display: none;">
			<div class="centerPopUpDiv">
				<div class="nav">
					<span class="menuButton" onclick="hideResponse(); afterLogin(); return false;">
						<g:message code="default.close.label" class="link" />
					</span>
					<div id="responseMsg"></div>
				</div>
			</div>
		</div>
		<div id="bottom">
			<div id="pie_menu" class="pie_menu">
				<ul>
					<li class="li_pie izq">
						<a href="http://www.yosy.org" class="link">
							Quiénes somos
						</a>
					</li>
					<li class="li_pie">
						<a href="mailto:hola@yosy.org" class="link">
							Contacto
						</a>
					</li>
					<li class="li_pie">
						<g:link controller="index" action="privacy_policy" class="link">
							Política de privacidad
						</g:link>
					</li>
					<li class="li_pie">
						<g:link controller="index" action="disclaimer" class="link">
							Aviso legal
						</g:link>
					</li>
				</ul>
			</div>
			<div id="social" class="pie_social">
			<a href="http://www.facebook.com/pages/YOSY/231026743579581"  style="text-decoration:none;">
				<img src="${resource(dir:'images',file:'pie_twitter.png')}"  />
			</a>
			<a href="http://twitter.com/#!/yosyorg" style="text-decoration:none;">
				<img src="${resource(dir:'images',file:'pie_facebook.png')}"  />
			</a>
			</div>
		</div>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>  
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
		<script type="text/javascript"
			src="${resource(dir:'js',file:'application.js',absolute:true)}">
		</script>
		<!-- <script src="http://192.168.1.130:8080/NewYosy/js/application.js" type="text/javascript"></script> -->
	</body>
</html>