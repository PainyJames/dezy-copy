
<g:javascript library="jquery" plugin="jquery" />
<div class="texto_privacy">
<div class="navigacion_nivel_dos">
	<g:link controller="index" class="navigacion_nivel_uno">
							Inicio
						</g:link>
		&nbsp; > Aviso Legal
</div>
<p>
	Este sitio web ha sido creado para la Asociaci&oacute;n YOSY con car&aacute;cter informativo y operacional. 
	El hecho de acceder a este sitio web implica el conocimiento y aceptaci&oacute;n de los siguientes t&eacute;rminos y condiciones: 
	Titularidad del sitio web El nombre de los dominios, http://www.dezy.es y http://www.dezi.es, est&aacute; registrado a favor de la Asociaci&oacute;n YOSY, 
	El domicilio social de YOSY queda establecido, a los efectos de la presente Informaci&oacute;n Legal, 
	en la Calle Doctor Casas, 20 Planta 1 de Zaragoza, en Zaragoza, Espa&ntilde;a. Ud. puede ponerse en contacto con nosotros en los siguientes n&uacute;meros y direcci&oacute;n: 
	Tel&eacute;fono (+34) 619 135 352  Correo Electr&oacute;nico <a href="mailto:hola@yosy.org">hola@yosy.org</a>
</p>

<p>
	<b>CONDICIONES DE USO DE SITIO WEB</b>
	<br/>

	<a href="http://www.dezy.es">http://www.dezy.es</a> y <a href="http://www.dezi.es">http://www.dezi.es</a></br>
	Este sitio web y todo su material pertenecen a la Asociaci&oacute;n YOSY. Este Sitio o cualquier parte del mismo no se podr&aacute; reproducir, 
	duplicar, copiar, vender, revender ni explotar para cualquier fin comercial o de otro tipo que no sea expresamente permitido por 
	la Asociaci&oacute;n YOSY. La modificaci&oacute;n de los materiales o el uso de los mismos para otro fin constituye una violaci&oacute;n de la ley del 
	Copyright y de otros derechos de marca registrada. Los contenidos y servicios incluidos y ofrecidos en este sitio web, no est&aacute;n 
	dirigidos a aquellas personas residentes en aquellas jurisdicciones donde no se encuentren autorizados. &uacute;nicamente se encuentran 
	comprendidas en el presente sitio web aquellas p&aacute;ginas que figuren dentro del mapa del sitio web. El usuario accede voluntariamente 
	a este sitio web. El acceso y navegaci&oacute;n en este sitio web implica aceptar y conocer las advertencias legales, condiciones y t&eacute;rminos 
	de uso contenidos en ella. El mero acceso no implica el establecimiento de ning&uacute;n tipo de relaci&oacute;n comercial entre la Asociaci&oacute;n YOSY y el usuario.
<p>

<p>
	<b>RESPONSABILIDAD LIMITADA</b>
	<br/>

	Bajo ninguna circunstancia la Asociaci&oacute;n YOSY, su corporaci&oacute;n, empleados, proveedores y aliados tecnol&oacute;gicos ser&aacute;n responsables ante 
	terceros por cualquier da&ntilde;o directo o indirecto, econ&oacute;mico, p&eacute;rdida de clientela o p&eacute;rdida de ganancias, resultado del uso de este Sitio Web.
<p/>

<p>
	<b>
	ENLACES CON OTROS SITIOS WEB</b>
	<br/>

	Aquellos sitios web a los que se suministran enlaces no est&aacute;n bajo el control de la Asociaci&oacute;n YOSY y esta entidad no se responsabiliza de los
	contenidos de los mismos. La Asociaci&oacute;n YOSY se reserva el derecho a eliminar cualquier enlace o programa de enlace en cualquier momento. 
	La Asociaci&oacute;n YOSY no promociona a las empresas o productos con las que tiene enlaces y se reserva el derecho a que conste as&iacute; en sus p&aacute;ginas web. 
	Si el usuario decide acceder a cualquiera de los sitios de terceros que tienen enlaces con este Sitio Web, lo har&aacute; bajo su propia responsabilidad.
</p>
<p>
	<b>
	LEYES APLICABLES</b>
	<br/>
 
La Asociaci&oacute;n YOSY controla este Sitio Web desde sus oficinas e instalaciones en Zaragoza, Espa&ntilde;a. La Asociaci&oacute;n YOSY no establece que los materiales de este Sitio Web sean adecuados ni que est&eacute;n disponibles para su uso en otros puntos y est&aacute; prohibido el acceso a ellos en los pa&iacute;ses en los que su contenido sea ilegal. Quienes accedan a este Sitio Web desde otros lugares, lo har&aacute;n por iniciativa propia y ser&aacute;n responsables del cumplimiento de las leyes locales aplicables. No se deben utilizar los materiales de forma que se incumplan las normas y las leyes de exportaci&oacute;n de Espa&ntilde;a.
</p>

<p>
	<b>
NOMBRE DE USUARIO Y CLAVE DE ACCESO</b>
	<br/>

El usuario es responsable de proteger su nombre de usuario y clave frente a usos y revelaciones no autorizadas. Cualquier uso de este Sitio Web empleando su nombre y clave de acceso ser&aacute; responsabilidad suya. Informe inmediatamente a la Asociaci&oacute;n YOSY en caso de que advierta cualquier uso o revelaci&oacute;n no autorizada de su clave de acceso.
</p>

<p>
	<b>
		MARCAS
	</b>
	<br/>

	Las marcas y logotipos que aparecen en esta p&aacute;gina Web pertenecen a la Asociaci&oacute;n YOSY y no pueden utilizarse en ning&uacute;n tipo de anuncio o de material publicitario relativos a la distribuci&oacute;n de cualquier informaci&oacute;n o contenido extra&iacute;do de esta p&aacute;gina Web sin el previo consentimiento por escrito del propietario legal de la marca.
	</br>
	Dezy es una marca de producto registrada por Asociaci&oacute;n YOSY, as&iacute; como el respectivo logo identificativo de la marca. Dicha marca, imagen y alusi&oacute;n a la misma queda amparada bajo las leyes de copyright y podr&aacute; ser perseguido seg&uacute;n la legislaci&oacute;n vigente.
</p>

<p>
	<b>INFORMACI&Oacute;N Y SU ACTUALIZACI&Oacute;N</b>
	</br>

La Asociaci&oacute;n YOSY hace los mayores esfuerzos para asegurar que la informaci&oacute;n que aparece en este Sitio Web sea correcta y se encuentre actualizada. No obstante, no se descartan errores tipogr&aacute;ficos u omisiones, por lo que el usuario no deber&aacute; considerar exacta la informaci&oacute;n sin comprobar antes tal exactitud directamente con la Asociaci&oacute;n YOSY. Ninguno de los contenidos de esta p&aacute;gina Web debe ser considerado como una afirmaci&oacute;n o un hecho irrefutable. La Asociaci&oacute;n YOSY no puede controlar el empleo que el usuario da a la informaci&oacute;n y por tanto no ser&aacute; responsable de ning&uacute;n tipo de da&ntilde;os o perjuicios, ya sean directos o indirectos, que puedan derivar del empleo de dicha informaci&oacute;n. En ning&uacute;n caso resultar&aacute;n afectados los derechos que, como consumidor, puedan corresponder legalmente al ciudadano.
</p>
<p>
	<b>CORRESPONDENCIA</b>
	</br>

Cualquier informaci&oacute;n dirigida a esta p&aacute;gina Web, excepto en las zonas restringidas, servicios, procesos y tributos, ser&aacute; considerada de naturaleza no confidencial y p&uacute;blica, con lo que podr&aacute; ser difundida, archivada o utilizada por la Asociaci&oacute;n YOSY para cualquier finalidad. No dirija a &eacute;sta p&aacute;gina Web ni desde la misma, ning&uacute;n tipo de informaci&oacute;n o contenido ilegal, difamatorio, o cualquier otro que pudiera dar lugar a responsabilidad civil o penal en el territorio al que esta p&aacute;gina Web se refiere. Con relaci&oacute;n a los datos recabados, se informa a los usuarios que tienen reconocidos los derechos de acceso, rectificaci&oacute;n, oposici&oacute;n y cancelaci&oacute;n de sus datos. Los referidos derechos podr&aacute;n ser ejercitados, bien por el usuario, bien por su representante, mediante la remisi&oacute;n de una solicitud escrita y firmada con el nombre y apellidos del usuario, fotocopia del DNI, as&iacute; como el contenido de la petici&oacute;n que desee realizar, a la siguiente direcci&oacute;n: Asociaci&oacute;n YOSY, Calle Doctor Casas 20 Planta 1, 50008 Zaragoza (Espa&ntilde;a)
</p>
<p>
	<b>MODIFICACI&Oacute;N DE LOS PRESENTES T&Eacute;RMINOS Y CONDICIONES</b>
	</br>
	La Asociaci&oacute;n YOSY puede revisar estas condiciones en cualquier momento mediante la actualizaci&oacute;n de esta publicaci&oacute;n. Dado el car&aacute;cter vinculante de las mismas, el usuario debe visitar esta p&aacute;gina cada cierto tiempo a fin de revisar las condiciones en vigor en cada momento. Los avisos o t&eacute;rminos legales que de forma expresa se indiquen en determinadas p&aacute;ginas de esta publicaci&oacute;n prevalecer&aacute;n sobre las estipulaciones previstas en estas condiciones.
	</br>
	</br>
<b>AVISO LEGAL</b>
</p>
<p>
	<b>1. CONDICIONES GENERALES Y ACEPTACI&Oacute;N.</b>
	</br>
	Estas condiciones generales (en adelante, las "Condiciones Generales") regulan el uso del servicio del WebSite Internet "http://www.dezy.es y http://www.dezi.es " (en adelante, el "WebSite") que la Asociaci&oacute;n YOSY, pone gratuitamente a disposici&oacute;n de los usuarios de Internet. La utilizaci&oacute;n del WebSite atribuye la condici&oacute;n de usuario del WebSite (en adelante, el "Usuario") y expresa la aceptaci&oacute;n plena y sin reservas del Usuario, de todas y cada una de las Condiciones en la versi&oacute;n publicada por la Asociaci&oacute;n YOSY en el momento mismo en que el Usuario acceda al WebSite. En consecuencia, el Usuario debe leer atentamente las Condiciones Generales cada vez que utilice el WebSite. Asimismo, la utilizaci&oacute;n del Servicio se encuentra sometida a todos los avisos e instrucciones puestos en conocimiento del Usuario por Asociaci&oacute;n YOSY, que completan lo previsto en estas Condiciones Generales en cuanto no se opongan a ellas.
</p>
<p>
	<b>2. OBJETO </b>
	</br>

A trav&eacute;s del WebSite, la Asociaci&oacute;n YOSY facilita a los Usuarios el acceso y la utilizaci&oacute;n de diversos servicios y contenidos puestos a disposici&oacute;n de los Usuarios por la asociaci&oacute;n, o por terceros usuarios del WebSite y/o por terceros proveedores de servicios y contenidos. La Asociaci&oacute;n YOSY se reserva el derecho a modificar unilateralmente, en cualquier momento y sin aviso previo, la presentaci&oacute;n y configuraci&oacute;n del WebSite, as&iacute; como los Servicios y las condiciones requeridas para utilizar el WebSite y los Servicios.
</p>
<p>
	<b>3. CONDICIONES DE ACCESO Y UTILIZACI&Oacute;N DEL WEBSITE</b>
	</br>


<b>3.1. Car&aacute;cter gratuito del acceso y utilizaci&oacute;n del WebSite. Claves de acceso.</b></br>


La prestaci&oacute;n del servicio de WebSite por parte de la Asociaci&oacute;n YOSY tiene car&aacute;cter gratuito para los Usuarios y no exige la previa suscripci&oacute;n o registro del Usuario. No obstante, la Asociaci&oacute;n YOSY reserva algunos de los Servicios ofrecidos a trav&eacute;s del WebSite a los usuarios previamente registrados. En estos casos, el usuario se compromete a elegir, indicar, usar y conservar su nombre de usuario (en adelante "login") y su contrase&ntilde;a (en adelante "password") (en adelante las "Claves de Acceso"). El Usuario no podr&aacute; elegir como login palabras, expresiones o conjuntos gr&aacute;fico-denominativos malsonantes, injuriosos, coincidentes con marcas, nombres comerciales, r&oacute;tulos de establecimientos, denominaciones sociales, expresiones publicitarias, nombres y seud&oacute;nimos de personajes de relevancia p&uacute;blica o famosos para cuya utilizaci&oacute;n no est&eacute; autorizado y, en general, contrarios a la ley o a las buenas costumbres generalmente aceptadas. La asignaci&oacute;n de las Claves de Acceso se produce de manera autom&aacute;tica con la elecci&oacute;n del usuario de sus propias Claves de Acceso. El Usuario se compromete a hacer un uso diligente de las Claves de Acceso, as&iacute; como a no ponerlas a disposici&oacute;n de terceros. El Usuario se compromete a comunicar a la Asociaci&oacute;n YOSY a la mayor brevedad la p&eacute;rdida o robo de las Claves de Acceso as&iacute; como cualquier riesgo de acceso a las mismas por un tercero.</br>
</br>
<b>3.2. Obligaci&oacute;n de hacer un uso correcto del WebSite y de los Servicios.</b></br>


El Usuario se compromete a utilizar el WebSite y los Servicios de conformidad con estas Condiciones Generales, la ley y las buenas costumbres generalmente aceptadas. El Usuario se obliga a abstenerse de utilizar el WebSite y los Servicios con fines o efectos il&iacute;citos, contrarios a lo establecido en estas Condiciones Generales, lesivos de los derechos e intereses de terceros, o que de cualquier forma puedan da&ntilde;ar, inutilizar, sobrecargar o deteriorar el WebSite y los Servicios o impedir la normal utilizaci&oacute;n o disfrute del WebSite y de los Servicios por parte de los Usuarios. 3.3. Uso correcto de los Contenidos El Usuario se obliga a usar los Contenidos de forma diligente, correcta y l&iacute;cita y, en particular, se compromete a abstenerse de (a) utilizar los Contenidos de forma, con fines o efectos contrarios a la ley y a las buenas costumbres generalmente aceptadas; (b) reproducir o copiar, distribuir, transformar o modificar los Contenidos, a menos que se cuente con la autorizaci&oacute;n del titular de los correspondientes derechos o ello resulte legalmente permitido; (c) suprimir, eludir o manipular el "copyright" y dem&aacute;s datos identificativos de los derechos de la Asociaci&oacute;n YOSY o de otros titulares incorporados a los Contenidos, as&iacute; como los dispositivos t&eacute;cnicos de protecci&oacute;n, las huellas digitales o cualesquiera mecanismos de informaci&oacute;n que pudieren integrar los Contenidos. El Usuario consiente y acepta voluntariamente que el uso del WebSite, de los Servicios y de los Contenidos tiene lugar, en todo caso, bajo su &uacute;nica y exclusiva responsabilidad.
</p>
<p>
	<b>4. EXCLUSI&Oacute;N DE GARANT&Iacute;AS Y DE RESPONSABILIDAD</b>
	</br>


<b>4.1. Exclusi&oacute;n de garant&iacute;as y de responsabilidad por el funcionamiento del WebSite y de los Servicios y de los propios Contenidos</b>
</br>
<br/>
<i>4.1.1. Disponibilidad y continuidad, utilidad y falibilidad</i></br>
La Asociaci&oacute;n YOSY no garantiza la disponibilidad y continuidad del funcionamiento del WebSite y de los Servicios.</br>

La Asociaci&oacute;n YOSY EXCLUYE CUALQUIER RESPONSABILIDAD POR LOS DA&Ntilde;OS Y PERJUICIOS DE TODA NATURALEZA QUE PUEDAN DEBERSE A LA FALTA DE DISPONIBILIDAD O DE CONTINUIDAD DEL FUNCIONAMIENTO DEL WEBSITE Y DE LOS SERVICIOS.</br>
</br>
<i>4.1.2.Privacidad y seguridad en la utilizaci&oacute;n del WebSite y de los Servicios</i></br>

La Asociaci&oacute;n YOSY no garantiza la privacidad y seguridad de la utilizaci&oacute;n del WebSite y de los Servicios y, en particular, no garantiza que terceros no autorizados accedan a las condiciones, caracter&iacute;sticas y circunstancias del uso que los Usuarios hacen del WebSite y de los Servicios. La Asociaci&oacute;n YOSY EXCLUYE TODA RESPONSABILIDAD POR LOS DA&ntilde;OS Y PERJUICIOS DE TODA NATURALEZA QUE PUDIERAN DEBERSE AL CONOCIMIENTO QUE PUEDAN TENER TERCEROS NO AUTORIZADOS DE LAS CONDICIONES, CARACTER&iacute;STICAS Y CIRCUNSTANCIAS DEL USO QUE LOS USUARIOS HACEN DEL PORTAL Y DE LOS SERVICIOS.</br>
</br>
<b>4.2. Exclusi&oacute;n de garant&iacute;as y de responsabilidad por los Contenidos</b></br>
<br/>

<i>4.2.1. Calidad.</i></br>

La Asociaci&oacute;n YOSY no controla ni garantiza la ausencia de virus ni de otros elementos en los Contenidos que puedan producir alteraciones en su sistema inform&aacute;tico (software y hardware) o en los documentos electr&oacute;nicos y ficheros almacenados en su sistema inform&aacute;tico. La Asociaci&oacute;n YOSY EXCLUYE CUALQUIER RESPONSABILIDAD POR LOS DA&ntilde;OS Y PERJUICIOS DE TODA NATURALEZA QUE PUEDAN DEBERSE A LA PRESENCIA DE VIRUS O A LA PRESENCIA DE OTROS ELEMENTOS EN LOS CONTENIDOS QUE PUEDAN PRODUCIR ALTERACIONES EN EL SISTEMA INFORM&aacute;TICO, DOCUMENTOS ELECTRONICOS O FICHEROS DE LOS USUARIOS.</br>
</br>
<i>4.2.2. Veracidad, exactitud, exhaustividad y actualidad</i></br>

La elaboraci&oacute;n de los contenidos y por tanto, las opiniones, consejos o cualquier otro tipo de informaci&oacute;n que integren los contenidos, ser&aacute; responsabilidad exclusiva de su autor o titular. La Asociaci&oacute;n YOSY EXCLUYE CUALQUIER RESPONSABILIDAD POR LOS DA&ntilde;OS Y PERJUICIOS DE TODA NATURALEZA QUE PUEDAN DEBERSE A LA FALTA DE VERACIDAD, EXACTITUD, EXHAUSTIVIDAD Y/O ACTUALIDAD DE LOS CONTENIDOS.</br>
</br>
<b>4.3. Exclusi&oacute;n de garant&iacute;as y de responsabilidad por la informaci&oacute;n, contenidos y servicios y alojados fuera del WebSite</b></br>


El WebSite pone a disposici&oacute;n de los Usuarios dispositivos t&eacute;cnicos de enlace (tales como, entre otros, links, banners, botones), directorios y herramientas de b&uacute;squeda que permiten a los Usuarios acceder a sitios web pertenecientes a y/o gestionados por terceros (en adelante, "Sitios Enlazados"). La instalaci&oacute;n de estos enlaces, directorios y herramientas de b&uacute;squeda en el WebSite tiene por &uacute;nico objeto facilitar a los Usuarios la b&uacute;squeda y acceso a la informaci&oacute;n, contenidos y servicios disponibles en Internet. La Asociaci&oacute;n YOSY no ofrece ni comercializa por s&iacute; la informaci&oacute;n, contenidos y servicios disponibles en los Sitios Enlazados, ni los controla previamente, aprueba, vigila ni hace propios. El Usuario, por tanto, debe extremar la prudencia en la valoraci&oacute;n y utilizaci&oacute;n de la informaci&oacute;n, contenidos y servicios existentes en los Sitios Enlazados. la Asociaci&oacute;n YOSY no tiene obligaci&oacute;n de controlar y por tanto, no controla la utilizaci&oacute;n que los Usuarios hacen del WebSite, de los Servicios y de los Contenidos que los Usuarios proporcionan sobre s&iacute; mismos a otros Usuarios.
</p>
<p>
	<b>5. DATOS DE CAR&Aacute;CTER PERSONAL</b>
	</br>

Para la utilizaci&oacute;n de determinados servicios, los usuarios deben proporcionar previamente a la Asociaci&oacute;n YOSY ciertos datos de car&aacute;cter personal, que ser&aacute;n sometidos a lo dispuesto en la Ley Org&aacute;nica 15/1999, de 13 de diciembre, que regula la Protecci&oacute;n de Datos de Car&aacute;cter Personal, y sus normas de desarrollo. Se informa al usuario de la posibilidad de ejercitar los derechos de acceso, rectificaci&oacute;n y cancelaci&oacute;n. Estos derechos se ejercer&aacute;n mediante solicitud escrita en tal sentido, dirigida a la Asociaci&oacute;n YOSY, indicando la referencia "Protecci&oacute;n Datos" la Asociaci&oacute;n YOSY se compromete a cumplir su obligaci&oacute;n de secreto de los datos de car&aacute;cter personal y su deber de guardarlos y adoptar&aacute; las medidas de &iacute;ndole t&eacute;cnica y organizativa necesarias para evitar su alteraci&oacute;n, p&eacute;rdida, tratamiento o acceso no autorizado, habida cuenta en todo momento el estado de la tecnolog&iacute;a.
</p>
<p>
	<b>6. PROPIEDAD INDUSTRIAL E INTELECTUAL </b>
	</br>

La Asociaci&oacute;n YOSY no concede ninguna licencia o autorizaci&oacute;n de uso de ninguna clase sobre sus derechos de propiedad industrial e intelectual o sobre cualquier otra propiedad o derecho relacionado con el WebSite, los Servicios o los Contenidos.
</p>
<p>
	<b>7. DENEGACI&Oacute;N Y RETIRADA DEL ACCESO AL WEBSITE Y/O A LOS SERVICIOS</b>
	</br>

La Asociaci&oacute;n YOSY se reserva el derecho a denegar o retirar el acceso al WebSite y/o a los Servicios, en cualquier momento y sin necesidad de preaviso a aquellos Usuarios que incumplan estas Condiciones.
</p>
<p>
	<b>8. DURACI&Oacute;N Y TERMINACI&Oacute;N</b>
	</br>

La prestaci&oacute;n del servicio de WebSite y de los dem&aacute;s Servicios tiene, en principio, una duraci&oacute;n indefinida. La Asociaci&oacute;n YOSY, no obstante, est&aacute; autorizado para dar por terminada o suspender la prestaci&oacute;n del servicio del WebSite y/o de cualquiera de los Servicios en cualquier momento, sin perjuicio de lo que se hubiere dispuesto al respecto en las correspondientes Condiciones Particulares. Cuando ello sea razonablemente posible, Asociaci&oacute;n YOSY advertir&aacute; previamente la terminaci&oacute;n o suspensi&oacute;n de la prestaci&oacute;n del servicio de Portal y de los dem&aacute;s Servicios.
</p>
<p>
	<b>9. LEY APLICABLE Y JURISDICCI&Oacute;N</b>
	</br>

Estas Condiciones Generales se rigen por la legislaci&oacute;n espa&ntilde;ola.
</p>
</div>