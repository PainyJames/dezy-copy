<div class="lista_deseos">
	<div class="navigacion_nivel_dos">
		<g:link controller="index" class="navigacion_nivel_uno">Inicio</g:link>
		&nbsp; > Ver Deseos >
		${fieldValue(bean: deseoInstance, field: "titulo")}
	</div>
	<div class="deseo_detalle">
		<div class="lista_deseo_imagen_marco">
			<g:if test="${deseoInstance.usuario.image == 1}">
				<img src="${resource(dir:'images',file:'cara_uno.png')}"
					alt="cara :D" onmousedown="return false;"
					class="lista_deseo_imagen" />
			</g:if>
			<g:elseif test="${deseoInstance.usuario.image == 2}">
				<img src="${resource(dir:'images',file:'cara_dos.png')}"
					alt="cara :D" onmousedown="return false;"
					class="lista_deseo_imagen" />
			</g:elseif>
			<g:else>
				<img src="${resource(dir:'images',file:'cara_tres.png')}"
					alt="cara :D" onmousedown="return false;"
					class="lista_deseo_imagen" />
			</g:else>
		</div>
		<div class="lista_deseo_nombre">
			${deseoInstance.usuario.nombre}
		</div>
		<div class="lista_deseo_titulo">
			<g:link action="whisDetail" controller="index"
				params="[idWhis: deseoInstance.id]" style="text-decoration: none;">
				${fieldValue(bean: deseoInstance, field: "titulo")}
			</g:link>
		</div>
		<div class="lista_deseo_descripcion">
			${fieldValue(bean: deseoInstance, field: "descripcion")}
		</div>

		<div class="lista_deseo_social">
			<div class="lista_deseo_cat">
				${deseoInstance.categoria?.nombre}
			</div>
			<div class="lista_deseo_loc">
				${deseoInstance.localidad?.nombre}
			</div>
			<!--  <div class="lista_deseo_comment">-->
			<div class="example-commentheading">
				<a style="padding: 0px 10px 0px 10px;" title="Ver comentarios."
					class="linkBlack"
					onclick="$('#divFormWish${deseoInstance.id}').toggle('slow'); return false;">
					${deseoInstance.comentarios.size()} comentarios
				</a>
			</div>
			<div id="divFormWish${deseoInstance.id}" class="arrow_box"
				style="display: none">
				<span class="equis_cerrar"
					onclick="$('#divFormWish${deseoInstance.id}').toggle('slow'); return false;">X</span>
				</br>
				<g:each in="${deseoInstance.comentarios}" status="u"
					var="comentarioInstance">
					<img src="${resource(dir:'images',file:'cara_uno.png')}"
						alt="cara :D" onmousedown="return false;"
						class="imagen_comentario" />
					<div class="texto_comentario">
						<span class="nombre_comentario"> ${comentarioInstance.autor}
						</span></br>
						${comentarioInstance.texto}</br>
						${comentarioInstance.fecha.format("dd/MM/yyyy - hh:mm")}</br>
					</div>
				</g:each>
				<div>
					<g:formRemote name="commentForm"
						url="[ controller: 'index', action: 'createComment']">
						<g:hiddenField name="id" value="${deseoInstance.id}" />
						<g:textArea name="texto" class="anadir_texto_comentario"></g:textArea>
						<g:submitToRemote name="botonAnadirComentario${deseoInstance.id}"
							class="anadir_comentario" controller="index"
							action="createComment" value="Añadir comentario"
							onFailure="showLogin(); return false;"
							onSuccess="showResponse('Comentario creado'); jQuery('[name=comentarios${deseoInstance.id}]').text('Gracias!'); clear('commentList${deseoInstance.id}'); return false;"
							update="commentList${deseoInstance.id}" />
						<img
							onclick="$('[name=botonAnadirComentario${deseoInstance.id}]').click()"
							src="${resource(dir:'images',file:'boton.png')}"
							style="cursor: pointer; height: 54px; width: 570px; margin: 0 0 0 3%;" />
						<div class="anadir_comentario_texto"
							onclick="setCode('$(\'[name=botonAnadirComentario${deseoInstance.id}]\').click()'); $('[name=botonAnadirComentario${deseoInstance.id}]').click()">
							Añadir Comentario</div>
					</g:formRemote>
				</div>
			</div>
			<div class="lista_deseo_twitter">
				<a href="https://twitter.com/share" class="twitter-share-button"
					data-text='"${fieldValue(bean: deseoInstance, field: "titulo")}"'
					data-via="dezy" data-size="large"> Tweet </a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			</div>
			<div class="lista_deseo_fb">
				<div id="fb-root"></div>
				<script>
					(function(d, s, id) {
						var js, fjs = d.getElementsByTagName(s)[0];
						if (d.getElementById(id)) return;
				  		js = d.createElement(s); js.id = id;
				  		js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1&appId=393740090668791";
				  		fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));
				</script>
				<div class="fb-like"
					data-href="/index/whisDetail?idWhis=${deseoInstance.id}"
					data-send="false" data-layout="box_count" data-width="70"
					data-show-faces="false" data-font="tahoma"></div>
			</div>
			<div class="lista_deseo_votos">
				<g:remoteLink class="imagen_votos" controller="index" action="vote"
					id="${deseoInstance.id}" onFailure="showLogin(); return false;"
					name='votar${deseoInstance.id}'
					onSuccess="showResponse(data); jQuery('[name=votos${deseoInstance.id}]').text('Gracias!'); return false;">
					<img
						onclick="setCode('$(\'[name=votar${deseoInstance.id}]\').click()'); $('[name=votar${deseoInstance.id}]').click()"
						src="${resource(dir:'images',file:'thumbs_up_azul_dcha.png')}"
						style="max-height: 30px; cursor: pointer; border: none;" />
				</g:remoteLink>
				<g:remoteLink class="texto_votos" controller="user"
					action="listUserVotes" id="${deseoInstance.id}"
					name="votos${deseoInstance.id}"
					onFailure="showResponse('Ha ocurrido un error'); return false;"
					onSuccess="showResponseHtml(data); return false;">
					${deseoInstance.votos.size()} votos
						</g:remoteLink>
			</div>
			<img class="lista_deseo_estrellas"
				src="${resource(dir:'images',file:'estrellitas.png')}">
		</div>
	</div>
</div>