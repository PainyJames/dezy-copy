
<g:javascript library="jquery" plugin="jquery" />
<div class="texto_privacy">
<div class="navigacion_nivel_dos">
	<g:link controller="index" class="navigacion_nivel_uno">
							Inicio
						</g:link>
		&nbsp; > Pol&iacute;tica de privacidad
</div>
<p>
	En cumplimiento de la LOPD, Ley 15/1999, de 13 de diciembre, de protecci&oacute;n de datos de car&aacute;cter personal, 
	se informa que los datos de car&aacute;cter personal que usted facilita, incluido su direcci&oacute;n de  correo electr&oacute;nico, 
	y que resultan necesarios para la formalizaci&oacute;n, gesti&oacute;n administrativa, ejecuci&oacute;n y desarrollo de la actividad propia de la 
	Asociaci&oacute;n sin &aacute;nimo de lucro YOSY (en lo sucesivo YOSY) se incluir&aacute;n en nuestros ficheros de datos personales, 
	cuyo &uacute;nico responsable y titular es YOSY.
</p>

<p>
	Asimismo, al remitir el interesado de forma voluntaria sus datos de car&aacute;cter personal a YOSY, autoriza expresamente 
	la utilizaci&oacute;n de los mismos a efectos de comunicaci&oacute;nes peri&oacute;dicas, incluyendo expresamente las que se realicen v&iacute;a 
	correo electr&oacute;nico o SMS que YOSY puede llevar a cabo con sus socios y posibles interesados inform&aacute;ndo de sus actividades, 
	noticias, proyectos, programas, as&iacute; como cuaquier otra oferta de productos y servicios relacionados con la actividad que se desarrolla. 
	Estos datos en ning&uacute;n caso son facilitados a otras empresas o personas ajenas a la organizaci&oacute;n.
</p>

<p>
	Puede ejercer sus derechos de acceso, rectificaci&oacute;n, cancelaci&oacute;n y oposici&oacute;n, dirigi&eacute;ndose por escrito mediante carta o 
	correo electr&oacute;nico a <a href="mailto:hola@yosy.org">hola@yosy.org</a>
</p>
<p>
	Para cumplir con lo establecido en la Ley 34/2002, de 11 de julio, de servicios de la sociedad de la informaci&oacute;n y 
	comercio electr&oacute;nico (LSSI-CE), a continuaci&oacute;n se indican los datos del titular de este sitio web: <br/>
	<b>ASOCIACI&Oacute;N YOSY</b> <br/>
	Calle Doctor Casas 20 Planta 1 <br/>
	50008 de Zaragoza (Espa&ntilde;a)<br/>
	CIF: G99333676 <br/>
	Asociaci&oacute;n inscrita en el Registro General de Asociaciones de la Comunidad Aut&oacute;noma de Arag&oacute;n, con el n&uacute;mero 01-Z-3290-2011.
</p>
<p>
	&copy; YOSY 2011 - Asociaci&oacute;n YOSY | Todos los derechos reservados
</p>