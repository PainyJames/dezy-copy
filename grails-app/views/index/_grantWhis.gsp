<g:javascript library="jquery" plugin="jquery" />
<div class="texto_legal">
<div class="navigacion_nivel_dos">
	<g:link controller="index" class="navigacion_nivel_uno">
							Inicio
						</g:link>
		&nbsp; > Conceder deseos
</div>
<div class="one_half">
<h2>El origen</h2>
<p>El verano pasado tuvimos la suerte de conocer a Luc&iacute;a, sus miedos, sus inquietudes y tambi&eacute;n su deseo.
Os dejamos con la historia de Luc&iacute;a:</p>
<iframe src="http://player.vimeo.com/video/34603360" width="400" height="225" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe> 
<p><a href="http://vimeo.com/34603360">Luc&iacute;a y el Mar</a> from <a href="http://vimeo.com/liquete">jorge liquete</a> on <a href="http://vimeo.com">Vimeo</a>.</p>
<br/>
<p>La alegr&iacute;a de poder ayudar a muchas "Luc&iacute;as" y a sus familias, es lo que nos ha movido a creer en este proyecto.</p></br>
<p>Si tu tambi&eacute;n te quieres contagiar de esa alegr&iacute;a e ilusi&oacute;n, pues conceder un deseo a otra persona a trav&eacute;s de <b>DEZY</b>.</p>
</div>
<div class="one_half_last">
<p>
<h2>&iquest;C&oacute;mo ayudar?</h2>
Si est&aacute;s interesado en conceder un deseo, un miembro de nuestra asociaci&oacute;n trabajar&aacute; directamente contigo en la selecci&oacute;n de un deseo y en llevarlo a cabo.
</p>
</br>
<p>
Hay diversas maneras de ayudar a conceder un deseo: financiaci&oacute;n total, financiaci&oacute;n parcial o trueque. Tambi&eacute;n puedes colaborar compartiendo un deseo, ya que la visibilidad de los deseos es muy importante.
</p>
</br>
<p>
Si quieres m&aacute;s informaci&oacute;n, nos encontrar&aacute;s en el (+34) 619 135 352 y en <a href="mailto:hola@yosy.org">hola@yosy.org</a>
</p>
</br>
<p>
<b>&iexcl;No te pierdas la experiencia de ayudar a cumplir un sue&ntilde;o!</b></p>
<img src="${resource(dir:'images',file:'conceder_deseo.png')}" alt="YOSY" class="img_conceder"/>
</div>
</div>