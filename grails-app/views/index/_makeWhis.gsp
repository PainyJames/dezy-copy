<%@ page import="org.yosy.Deseo"%>
<%@ page import="org.yosy.Localidad"%>
<%@ page import="org.yosy.Categoria"%>
<link rel="stylesheet" href="${resource(dir:'js',file:'chosen/chosen.css')}" />
<g:javascript library="jquery" plugin="jquery" />
<div class="pedir_deseo">
	<g:formRemote url="[ controller: 'index', action: 'save']" name="login" on401="showLogin(); return false;" onSuccess="return false;">
		<div class="caja_deseo">
			<div class="titulo_deseo">Pide un deseo</div>
			<div class="cuerpo_deseo">
				<div id="firstStep">
					<g:textArea name="descripcion"
						value="${deseoInstance?.descripcion}" />
					<div class="tip_deseo">
						Ejemplo: ir al circo, ver los cisnes, comer paella...
					</div>
				</div>
				<div id="secondStep">
					<div>
						<div class="titulo_label_form_deseo">
							<span>
								Título
							</span>
							<g:textField name="titulo" value="${deseoInstance?.titulo}" class="titulo_form_deseo"/>
						</div>
						<div class="loc_form_deseo">
							<span class="loc_label_form_deseo">
								Localidad
							</span>
							<select data-placeholder="Elige la localidad..." class="chzn-select" name="localidad" id="localidad" style="width:275px; margin-right: 15px; margin-top: 10px;">
								<option value="" ></option>
								<g:each in="${localidad}" status="i" var="loc">
									<option value="${loc.nombre}" >${loc.nombre}</option> 
								</g:each>
							</select>
						</div>
						<div class="cat_form_deseo">
							<span class="cat_label_form_deseo">
								Categoría
							</span>
							<select data-placeholder="Elige la categoría..." class="chzn-select" name="categoria" id="categoria" style="width:275px; margin-right: 15px; margin-top: 10px;">
								<option value="" ></option>
								<g:each in="${categoria}" status="i" var="cat">
									<option value="${cat.nombre}" >${cat.nombre}</option> 
								</g:each>
							</select>
						</div>
					</div>
					<div class="tip_deseo" >	
						Ayudanos a entender y localizar tu deseo :) 
						<div style="display:none;">	
							<g:submitToRemote id="botonEnviarRemote" style="visibility:hidden" url="[controller: 'index', action: 'save']" onFailure="showLogin()" onSuccess="showResponseRedirect('Deseo creado',hostName+'index/list'); " name="create" class="rightButton" value="${message(code: 'default.button.send.label', default: 'Enviar')}" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="firstStepButton" onclick="$('#firstStep').hide(); $('#firstStepButton').hide(); $('#secondStep').show(); $('#secondStepButton').show(); return false;" class="enviar_deseo">
			<img src="${resource(dir:'images',file:'boton.png')}" style="width: 400px; height: 47px; cursor: pointer;" />
			<div class="continuar_deseo">
				Continuar
			</div>
		</div>
		<div id="secondStepButton" style="display: none" class="enviar_deseo">
			<img src="${resource(dir:'images',file:'boton.png')}" style="width: 190px; height: 47px; margin: 0 3% 0 0; cursor: pointer;" />
			<img src="${resource(dir:'images',file:'boton.png')}" style="width: 190px; height: 47px; cursor: pointer;" />
			<div class="continuar_paso_dos_deseo">
				<span onclick="$('#firstStep').show(); $('#firstStepButton').show(); $('#secondStep').hide(); $('#secondStepButton').hide(); return false;" class="atras_deseo">
					Volver
				</span>
				<span onclick="setCode('$(\'#botonEnviarRemote\').click();'); $('#botonEnviarRemote').click()" class="enviar_fin_deseo">
					Enviar
				</span>
			</div>
		</div>
	</g:formRemote>
</div>
<div class="instructions">
	<div class="title_instructions">
		¿Cómo funciona?
	</div>
	<div class="content_instructions">
		<ul class="pasos_instructions">
			<li class="paso_instructions paso_uno">
				<img alt="paso uno" src="${resource(dir:'images',file:'marco.png')}" class="image_instructions">
				<div class="pie_instructions">
					<div class="titulo_pie_instructions">Pide un deseo</div>
					Lucía quiere ir al mar y lo ha escrito en Dezy</br>
				</div>
			</li>
			<li class="paso_instructions paso_dos">
				<img alt="paso uno" src="${resource(dir:'images',file:'marco.png')}" class="image_instructions">
				<div class="pie_instructions">
					<div class="titulo_pie_instructions">
						Compártelo hasta que alguien lo lea
					</div>
					Hugo, Andrea y Pablo han visto el deseo de Lucía
				</div>
			</li>
			<li class="paso_instructions paso_tres">
				<img alt="paso uno" src="${resource(dir:'images',file:'marco.png')}" class="image_instructions">
				<div class="pie_instructions">
					<div class="titulo_pie_instructions">
						Hazlo realidad
					</div>
					Gracias a Dezy, Lucía puede conocer el mar</br>
				</div>
			</li>
		</ul>
	</div>
</div>
<script src="${resource(dir:'js',file:'chosen/chosen.jquery.js')}"
	type="text/javascript"></script>
<script type="text/javascript"> 
  	$(".chzn-select").chosen({no_results_text: "No se han encontrado resultados para:"});
</script>