package org.yosy

class Localidad {

	String nombre
	
	static hasMany =	[
		deseos:Deseo
	]
	
	static mapping = { id generator: 'native' }
	
	static constraints = {
		deseos(nullable: true)
	}
	
}
