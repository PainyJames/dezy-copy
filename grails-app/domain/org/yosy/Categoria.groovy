package org.yosy

class Categoria {

	String nombre

	static hasMany =	[
		deseos:Deseo
	]
	
	static mapping = { id generator: 'native' }
	
	static constraints = {
		deseos(nullable: true)
	}
	
}
