package org.yosy

class Comentario implements Comparable{
	
	static belongsTo = Deseo
	
	String email
	String autor
	String texto
	Date fecha

	int compareTo(obj) {
		fecha.compareTo(obj.fecha)
	}
	
	static mapping = {
		id generator: 'native'
		sort "fecha", order: 'desc'
	}
	
	static constraints = {
	}
}
