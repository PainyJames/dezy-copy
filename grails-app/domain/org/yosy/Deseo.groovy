package org.yosy

class Deseo {

	static hasMany =	[
		comentarios:Comentario,
		votos:Voto
	]

	static belongsTo = [usuario:Usuario,categoria:Categoria,localidad:Localidad]

	String titulo
	String descripcion
	Date fechaModificacion
	Date fechaCreacion
	Estado estado
	SortedSet comentarios

	static mapping = {
		id generator: 'native'
		sort fechaCreacion:"desc"
	}

	static constraints = {
		titulo(nullable:true)
		categoria(nullable:true)
		localidad(nullable:true)
	}
}
