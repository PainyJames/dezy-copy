package org.yosy

import org.springframework.transaction.annotation.Transactional

class LocationService {

    static transactional = true

	@Transactional(readOnly = true)
    def autocompleteLocation(params) {
		def results = Localidad.withCriteria {
			ilike 'nombre', params.term + '%'
		}
		return results
    }
	
	@Transactional(readOnly = true)
	def list() {
		return Localidad.listOrderByNombre()
	}
}
