package org.yosy

import org.springframework.transaction.annotation.Transactional

class WhisesService {

	static transactional = true

	@Transactional(readOnly = true)
	def listWhises(params) {
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		params.order = params.order ? params.order : 'desc'
		params.sort = params.sort ? params.sort : 'fechaCreacion'
		return Deseo.list(params)
	}

	@Transactional(readOnly = true)
	def countWhises() {
		return Deseo.count()
	}
	
	@Transactional(readOnly = true)
	def get(idDeseo) {
		def deseo = Deseo.get(idDeseo)
		return deseo
	}

	@Transactional
	def save(params, email) {
		try {
			def now = new java.util.Date()
			def usuarioInstance = Usuario.findByEmail(email)
			def title = params['titulo']
			def categoria = params['categoria']
			//Se obtiene o crea la categoria
			def bdcategoria = null
			if (categoria != "") {
				bdcategoria = Categoria.findByNombre(categoria)
				if(bdcategoria == null)
				{
					bdcategoria = new Categoria(nombre:categoria)
					bdcategoria.save()
				}
			}
			//Se obtiene o crea la categoria
			def localidad = params['localidad']
			def bdlocalidad = null
			if(localidad != "") {
				bdlocalidad = Localidad.findByNombre(localidad)
				if(bdlocalidad == null)
				{
					bdlocalidad = new Localidad(nombre:localidad)
					bdlocalidad.save()
				}
			}
			if(title == "")
				title = params['descripcion'][0..60]
			def deseoInstance = new Deseo(titulo:params['titulo'],descripcion:params['descripcion'],fechaCreacion:now,fechaModificacion:now,estado:Estado.CREADO)
			
			if(bdcategoria != null)
			{
				if(bdcategoria.addToDeseos(deseoInstance).save() == false)
					return false
			}
			
			if(bdlocalidad != null)
			{
				if(bdlocalidad.addToDeseos(deseoInstance).save() == false)
					return false
			}
			
			if(usuarioInstance.addToDeseos(deseoInstance).save(flush:true)) {
				return true
			}
			return false
		}catch(e) {
			throw e
		}
	}
}
