package org.yosy

import org.springframework.transaction.annotation.Transactional

class UsersService {

	static transactional = true

	@Transactional
	def save(params) {
		try {
			def now = new java.util.Date()
			def usuarioInstance = new Usuario(params)
			usuarioInstance.save()
		}
		catch(e) {
			throw e
		}
	}

	@Transactional
	def register(params, utilities) {
		try {
			def usuarioInstance = new Usuario(params)
			def passwordRepeat = params["passwordRepeat"]
			usuarioInstance.password = params["passwordRegistro"]
			usuarioInstance.fechaAlta = new java.util.Date()
			if (passwordRepeat.equals(usuarioInstance.password)) {
				//Codificamos la contrase�a antes de introducirla en base de datos
				usuarioInstance.password = org.yosy.utils.Utilities.codMD5(usuarioInstance.password)
				usuarioInstance.estadoUsuario = EstadoUsuario.NOREGISTRADO
				usuarioInstance.tipoUsuario = TipoUsuario.USUARIO
				usuarioInstance.image = utilities.rnd(3)
				if (usuarioInstance.save(flush: true)) {
					//Se necesita hacer esto para parir el hash (s�, ya me jode ya...)
					def timestamp = new java.sql.Timestamp(usuarioInstance.fechaAlta.getTime()).toString()
					timestamp = timestamp.substring(0,timestamp.length()-3) +"0"
					//
					utilities.sendEmail(usuarioInstance.email, "Acaba de iniciar un proceso de registro en http://www.dezy.org."
							+"\nPara confirmar el mismo, s&oacute;lo tiene que clicar sobre el siguiente <a href='http://dezy.yosy.cloudbees.net/user/activate?email="+usuarioInstance.email+"&c="+org.yosy.utils.Utilities.codMD5(usuarioInstance.password+timestamp+usuarioInstance.email)+"'>enlace</a>"
							,"Confirmar registro yosy.org",true)
					return true
				}
				else
				{
					return false
				}
			}
			return false
		}
		catch(e) {
			throw e
		}
	}


	@Transactional(readOnly = true)
	def login(params) {
		try {
			def email = params?.email
			if(email==null || email.length() == 0)
			{
				return null
			}
			else {
				def usuarioInstance = Usuario.findByEmail(email)
				if(usuarioInstance != null && usuarioInstance.password == org.yosy.utils.Utilities.codMD5(params['password']))
				{
					return usuarioInstance
				}
				else
				{
					return null
				}
			}
		}
		catch(e) {
			throw e
		}
	}
	
	@Transactional
	def activate(params, utilities) {

		def correo = params['email']
		def md5 = params['c']
		def usuarioInstance = Usuario.findByEmail(correo)
		def md5C = org.yosy.utils.Utilities.codMD5(usuarioInstance.password+usuarioInstance.fechaAlta.toString()+usuarioInstance.email)
		if(md5C.compareTo(md5) == 0)
		{
			usuarioInstance.estadoUsuario = EstadoUsuario.REGISTRADO
			usuarioInstance.save(flush:true)
			utilities.sendEmail(correo, "Su registro en Dezy ha sido completado satisfactoriamente.","Registro completado",true)
			return true
		}
		return false
	}



	@Transactional(readOnly = true)
	def listUserVotes(params)
	{
		def idDeseo = new Long(params['id'])
		
		def usersList = Usuario.findAll("\
			from Usuario as u \
			where u.email in ( \
			select v.email from Deseo d \
			inner join d.votos v \
			where d.id = ?) ", [idDeseo])
			
		
		return usersList
		
	}

	@Transactional(readOnly = true)
	def existsEmail(email) {

		def user = Usuario.findByEmail(email)

		if (user == null)
		{
			return false
		}

		return true
	}

	@Transactional(readOnly = true)
	def existsNick(nick) {

		def user = Usuario.findByNombre(nick)

		if (user == null)
		{
			return false
		}

		return true
	}
	
}
