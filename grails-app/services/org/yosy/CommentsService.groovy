package org.yosy

import org.springframework.transaction.annotation.Transactional

class CommentsService {
	static transactional = true

	@Transactional(readOnly = true)
	def listComments(params) {
		def idDeseo = new Long(params['id'])
		def comentarios = Comentario.findAll("from Comentario c where c.id in (select cd.id from Deseo as d inner join d.comentarios cd where d.id = ?) order by c.fecha desc ",idDeseo)
		return comentarios
	}
	
	@Transactional(readOnly = true)
	def listCommentsAfterDate(params) {
		def idDeseo = new Long(params['idWish'])
	    def date = new java.text.SimpleDateFormat("dd/MM/yyyy - HH:mm:ss").parse(params['date'])
		def comentarios = Comentario.findAll("from Comentario c where c.id in (select cd.id from Deseo as d inner join d.comentarios cd where d.id =:id and c.fecha > :fecha) order by c.fecha asc ",[id:idDeseo,fecha:date])
		return comentarios
	}

	@Transactional
	def save(params, email,utilities) {
		try {
			def now = new java.util.Date()
			def usuarioInstance = Usuario.findByEmail(email)
			def comentario = new Comentario(params)
			comentario.fecha = now
			comentario.autor = usuarioInstance.nombre
			comentario.email = usuarioInstance.email
			def idDeseo = new Long(params['id'])
			def deseo = Deseo.get(idDeseo)
			if(deseo.addToComentarios(comentario).save(flush:true)) {
				def usuarioDeseo = deseo.usuario
				utilities.sendEmail(usuarioDeseo.email ,"El usuario con correo "+email+" ha comentado el deseo: "+deseo.titulo, "Comentario recibido",true)
				return true
			}
			return false
		}catch(e) {
			throw e
		}
	}
}
