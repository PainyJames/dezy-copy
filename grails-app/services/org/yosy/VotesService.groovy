package org.yosy

import org.springframework.transaction.annotation.Transactional

class VotesService {

	static transactional = true

	@Transactional()
	def vote(email, idDeseo,utilities) {
		def deseoList = Deseo.withCriteria {
			and {
				idEq (idDeseo)
				votos { eq ("email", email) }
			}
		}
		def existeVoto = deseoList.size() > 0

		//Si no se ha votado por ese deseo, se a�ade
		if(!existeVoto)
		{
			def deseoInstance = Deseo.get(idDeseo).addToVotos(new Voto(email: email)).save(flush:true)
			def numVotos = deseoInstance.votos.size()
			def usuario = deseoInstance.usuario
			utilities.sendEmail(usuario.email,"El usuario con correo "+email+" ha votado el deseo: "+deseoInstance.titulo, "Voto recibido" ,true)
			return numVotos+" VOTOS"
		}
		else
			return "Ya has votado este deseo"
	}

}
