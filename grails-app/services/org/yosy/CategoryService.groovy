package org.yosy

import org.springframework.transaction.annotation.Transactional

class CategoryService {

    static transactional = true

	@Transactional(readOnly = true)
    def autocompleteCategory(params) {
		def results = Categoria.withCriteria {
			ilike 'nombre', params.term + '%'
		}
		return results
    }
	
	@Transactional(readOnly = true)
	def list() {
		return Categoria.listOrderByNombre()
	}
}
