var hostName = "http://www.dezy.es/";

var _gaq = _gaq || [];
_gaq.push([ '_setAccount', 'UA-32152087-1' ]);
_gaq.push([ '_trackPageview' ]);

(function() {
	var ga = document.createElement('script');
	ga.type = 'text/javascript';
	ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl'
			: 'http://www')
			+ '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(ga, s);
})();

function showLogin() {
	isLogin = true;
	$('#loginPopUp').fadeIn();
	$('#backgroundDivPopUp').fadeIn();
}

function hideLogin() {
	$('#loginPopUp').fadeOut();
	$('#backgroundDivPopUp').fadeOut();
	// Vaciamos todos los campos.
	$("#email").val("");
	$("#passwordRegistro").val("");
	$("#email").val("");
	$("#passwordrepeat").val("");
}

function hideResponse() {
	$('#responsePopUp').fadeOut();
	$('#backgroundDivPopUp').fadeOut();
}

function showResponse(text) {
	$('#responsePopUp').fadeIn();
	$('#responseMsg').text(text);
	$('#backgroundDivPopUp').fadeIn();
}

function showResponseRedirect(text, redirect) {
	$('#responsePopUp').fadeIn();
	$('#responseMsg').text(text);
	$('#backgroundDivPopUp').fadeIn();
	setCode('window.location = "'+redirect+'";')
	isRedirect = true;
}

function showResponseHtml(html) {
	$('#responsePopUp').fadeIn();
	$('#responseMsg').html(html);
	$('#backgroundDivPopUp').fadeIn();
}

function loginLink() {
	$('#loginLink').css('display', 'none');
	$('#logoutLink').css('display', 'inline-block');
}

function logoutLink() {
	showResponse('Hasta otra :)');
	$('#loginLink').css('display', 'inline-block');
	$('#logoutLink').css('display', 'none');
}

function clear(id) {
	$('#' + id).html('');
}

// Mostramos un mensaje en la pantalla de login.
// Puede ser que es correcto, que no, validaciones, etc.
function ShowLoginError(error) {
	// Mostramos el mensaje correcto.
	if (error == "ErrorLogin") {
		$("#lblErrorLogin").show();
		$("#lblErrorPasswordVacio").hide();
		$("#lblErrorEmailVacio").hide();
	} else if (error == "PassVacio") {
		$("#lblErrorLogin").hide();
		$("#lblErrorPasswordVacio").show();
		$("#lblErrorEmailVacio").hide();
	} else if (error == "EmailVacio") {
		$("#lblErrorLogin").hide();
		$("#lblErrorPasswordVacio").hide();
		$("#lblErrorEmailVacio").show();
	}
}

// Para ver si el password coincide.
$('#passwordRegistro').change(function() {
	if (PasswordsCoinciden()) {
		// Coinciden.
		$("#passwordRegistro").removeClass("errorValidacionLogin");
		$("#passwordRepeat").removeClass("errorValidacionLogin");
		$("#lblErrorRegisterPass").hide();
	} else {
		// Son distintas.
		$("#passwordRegistro").addClass("errorValidacionLogin");
		$("#passwordRepeat").addClass("errorValidacionLogin");
		$("#lblErrorRegisterPass").show();
	}
});
$('#passwordRepeat').change(function() {
	if (PasswordsCoinciden()) {
		// Coinciden.
		$("#passwordRegistro").removeClass("errorValidacionLogin");
		$("#passwordRepeat").removeClass("errorValidacionLogin");
		$("#lblErrorRegisterPass").hide();
	} else {
		// Son distintas.
		$("#passwordRegistro").addClass("errorValidacionLogin");
		$("#passwordRepeat").addClass("errorValidacionLogin");
		$("#lblErrorRegisterPass").show();
	}
});

// Function para comprobar si los passwords coinciden.
function PasswordsCoinciden() {
	if (($("#passwordRegistro").val() != "")
			&& ($("#passwordRepeat").val() != "")) {
		// Los dos tienen algo escrito.
		if ($("#passwordRegistro").val() == $("#passwordRepeat").val()) {
			// Las contrase�as coinciden.
			return true;
		} else {
			// Las contrase�as son distintas.
			return false;
		}
	} else {
		// Devolvemos true, porque no tienen valor.
		return true;
	}
}

// Para el boton verdadero.
$("#btnRegister").hover(function() {
	if (RegisterValidations()) {
		// Son correctas.
		$("#btnFakeRegister").hide();
		$("#btnRegister").show();
	} else {
		// Son INcorrectas.
		$("#btnFakeRegister").show();
		$("#btnRegister").hide();
	}
}, function() {
	if (RegisterValidations()) {
		// Son correctas.
		$("#btnFakeRegister").hide();
		$("#spanBtnRegister").show();
	} else {
		// Son INcorrectas.
		$("#btnFakeRegister").show();
		$("#spanBtnRegister").hide();
	}
});

// Para el boton falso.
$("#btnFakeRegister").hover(function() {
	if (RegisterValidations()) {
		// Son correctas.
		$("#btnFakeRegister").hide();
		$("#spanBtnRegister").show();
	} else {
		// Son INcorrectas.
		$("#btnFakeRegister").show();
		$("#spanBtnRegister").hide();
	}
}, function() {
	if (RegisterValidations()) {
		// Son correctas.
		$("#btnFakeRegister").hide();
		$("#btnRegister").show();
	} else {
		// Son INcorrectas.
		$("#btnFakeRegister").show();
		$("#btnRegister").hide();
	}
});

$('#btnFakeRegister').click(function() {
	showResponse('Corrija los errores para continuar.');
});

// Si todos los campos de registro estan completos.
function RegisterFieldsCompleted() {
	// Quitamos todos los rojos.
	$(".emailRegistro").removeClass("errorValidacionLogin");
	$("#nombre").removeClass("errorValidacionLogin");
	$("#passwordRegistro").removeClass("errorValidacionLogin");
	$("#passwordRepeat").removeClass("errorValidacionLogin");

	var existsEmptyFields = false;
	if ($(".emailRegistro").val() == "") {
		existsEmptyFields = true;
		$(".emailRegistro").addClass("errorValidacionLogin");
	} else if ($("#nombre").val() == "") {
		existsEmptyFields = true;
		$("#nombre").addClass("errorValidacionLogin");
	} else if ($("#passwordRegistro").val() == "") {
		existsEmptyFields = true;
		$("#passwordRegistro").addClass("errorValidacionLogin");
	} else if ($("#passwordRepeat").val() == "") {
		existsEmptyFields = true;
		$("#passwordRepeat").addClass("errorValidacionLogin");
	}

	if (existsEmptyFields != true) {
		// Estan todos rellenos.
		$("#lblErrorRegisterCamposVacios").hide();
		return true;
	} else {
		// Hay alguno vacio.
		$("#lblErrorRegisterCamposVacios").show();
		return false;
	}
}

// Para comprobar las validaciones de todos y activar o desactivar el boton.
function RegisterValidations() {
	// Que los campos estan rellenos.
	if (RegisterFieldsCompleted() == false) {
		// Hay un fallo.
		return false;
	} else if (PasswordsCoinciden() == false) // Si los passwords coindicen.
	{
		// Hay un fallo.
		return false;
	} else if (ValidateEmail() == false) // Si el email es correcto en
	// formato.
	{
		// Hay un fallo.
		return false;
	} else {
		return true;
	}
}

$("#nombre").change(function() {
	// Mostramos la imagen de carga.
	$("#imgLoadingNick").show();
	// Comprobamos que no este puesto el nick.
	ExistsNick("http://www.dezy.es/index/existsNick", $("#nombre").val());
});

$(".emailRegistro").change(function() {
	// Mostramos la imagen de carga.
	$("#imgLoadingEmail").show();
	// Comprobamos que no este puesto.
	ExistsEmail(hostName + "index/existsEmail", $(".emailRegistro").val());
	if (ValidateEmail()) {
		// El formato del email es correcto.
		$(".emailRegistro").removeClass("errorValidacionLogin");
		$("#lblErrorRegisterEmail").hide();
	} else {
		// El formato del email NO es correcto.
		$(".emailRegistro").addClass("errorValidacionLogin");
		$("#lblErrorRegisterEmail").show();
	}
});

function ExistsEmail(url, email) {
	$.ajax({
		type : "GET",
		data : "email=" + email,
		url : url,
		success : function(exists) {
			if (exists == "true") {
				// Existe el email.
				$(".emailRegistro").addClass("errorValidacionLogin");
				$("#lblErrorEmailRepetido").show();
				$(".emailRegistro").removeClass("okValidacionLogin");
			} else {
				// No existe.
				$(".emailRegistro").removeClass("errorValidacionLogin");
				$("#lblErrorEmailRepetido").hide();
				$(".emailRegistro").addClass("okValidacionLogin");
			}
			// Ocultamos la imagen de carga.
			$("#imgLoadingEmail").hide();
		},
		error : function(xhr, ajaxOptions, thrownError) {
			alert("ERROR");
			alert(xhr.status);
			alert(thrownError);
		},
		complete : function() {
		}
	});
}

function ExistsNick(url, nick) {
	$.ajax({
		type : "GET",
		data : "nick=" + nick,
		url : url,
		success : function(exists) {
			if (exists == "true") {
				// Existe el nick.
				$("#lblErrorNickRepetido").show();
				$("#nombre").addClass("errorValidacionLogin");
				$("#nombre").removeClass("okValidacionLogin");
			} else {
				// No existe.
				$("#lblErrorNickRepetido").hide();
				$("#nombre").removeClass("errorValidacionLogin");
				$("#nombre").addClass("okValidacionLogin");
			}
			// Ocultamos la imagen de carga.
			$("#imgLoadingNick").hide();
		},
		error : function(xhr, ajaxOptions, thrownError) {
			alert("ERROR");
			alert(xhr.status);
			alert(thrownError);
		},
		complete : function() {
		}
	});
}

/**
 * DHTML email validation script. Courtesy of SmartWebby.com
 * (http://www.smartwebby.com/dhtml/)
 */

function echeck(str) {

	var at = "@"
	var dot = "."
	var lat = str.indexOf(at)
	var lstr = str.length
	var ldot = str.indexOf(dot)
	if (str.indexOf(at) == -1) {
		return false;
	}

	if (str.indexOf(at) == -1 || str.indexOf(at) == 0
			|| str.indexOf(at) == lstr) {
		return false;
	}

	if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0
			|| str.indexOf(dot) == lstr) {
		return false;
	}

	if (str.indexOf(at, (lat + 1)) != -1) {
		return false;
	}

	if (str.substring(lat - 1, lat) == dot
			|| str.substring(lat + 1, lat + 2) == dot) {
		return false;
	}

	if (str.indexOf(dot, (lat + 2)) == -1) {
		return false;
	}

	if (str.indexOf(" ") != -1) {
		return false;
	}

	return true;
}

function ValidateEmail() {
	var emailID = $(".emailRegistro").val();
	if ((emailID == null) || (emailID == "")) {
		return false;
	}
	if (echeck(emailID) == false) {
		return false;
	}
	return true
}

function EnterSubmit(event, codigo) {
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if (keycode == '13') {
		$.globalEval(codigo);
	}
}

// Funciones y var usadas para ejecutar código tras el login
var code = '';
var isLogin = false;
var isRedirect = false;
function setCode(c) {
	code = c;
}
function afterLogin() {
	if ((isLogin == true || isRedirect == true) && code != '') {
		eval(code);
		setCode('');
		isLogin = false;
		isRedirect = false;
	}
}
//

function updateComments(idWish, date) {
	var d;
	if (date.size() == 0)
		d = "";
	else
		d = date.text();

	$.ajax({
		type : "GET",
		data : "idWish=" + idWish + "&date=" + d,
		url : hostName + "index/listCommentsAfterDate/",
		success : function(comments) {
			updateCommentsSuccess(comments, idWish);
		},
		error : function(xhr, ajaxOptions, thrownError) {
		},
		complete : function() {
		}
	});
}
function updateCommentsSuccess(comments, idWish) {
	for ( var u = 0; u < comments.length; u++) {
		if ($('#commentWishId' + comments[u].id).size() == 0) {
			var newCommentImg = $(".imagen_comentario")[0].outerHTML;
			var newCommentDiv = "<div id='commentWishId"
					+ comments[u].id
					+ "' class=\"texto_comentario\"><span class=\"nombre_comentario\"> "
					+ comments[u].autor
					+ "</span></br>"
					+ comments[u].texto
					+ "</br>"
					+ comments[u].fecha.replace(/-/g, '/').replace('T', ' - ')
							.replace('Z', '') + "</br></div>";
			$("#newComments"+idWish).append(newCommentImg + newCommentDiv);

			var dateElement = $("#lastDateComment" + idWish);
			if (dateElement.size() == 0)
				$("#newComments"+idWish).append("<div id=\"lastDateComment"
						+ idWish
						+ "\" style=\"display: none\">"
						+ comments[u].fecha.replace(/-/g, '/').replace('T',
								' - ').replace('Z', '.0') + "</div>");
			else
				dateElement.text(comments[u].fecha.replace(/-/g, '/').replace(
						'T', ' - ').replace('Z', '.0'));
		}
	}
}